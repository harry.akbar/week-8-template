from os import environ


APP_ENV = environ.get('APP_ENV', 'development')
DEBUG = environ.get('DEBUG', 'true') == 'true'
TELEGRAM_BOT_TOKEN = environ.get('TELEGRAM_BOT_TOKEN', '344158091:AAEbTQw7dZ-ulmg59-PF6gHM4iFOFyiq3KE')
LOG_LEVEL = environ.get('LOG_LEVEL', 'DEBUG')
WEBHOOK_HOST = environ.get('WEBHOOK_HOST', 'HTTPS://csui15zodiac.herokuapp.com/bot')
WEBHOOK_URL = environ.get('WEBHOOK_URL', 'HTTPS://csui15zodiac.herokuapp.com')